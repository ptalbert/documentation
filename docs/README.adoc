---
title: 'Red Hat KWF: Quick Start Guide (QSG)'
date: 2021-07-20T19:30:08+10:00
draft: false
weight: 1
summary: Entry point.
---

= Red Hat KWF: Quick Start Guide (QSG)

{{< hint info >}}
prarit Feb 5 2021: I can definitely use some help with this document.  I’m too close to the main KWF documentation to offer any sort of sane Quick Start Guide.  If you have any suggestions, or things that should be changed, please feel free to suggest changes!  Thanks :)
{{< /hint >}}

These are simple instructions and command-line examples to start contributing to the Red Hat kernel.  This document is not meant to be an introduction to the Red Hat Kernel Workflow.  Questions on this document likely can be answered in the https://red.ht/kernel_workflow_doc[main KWF documentation].

Problems or concerns with the workflow can be addressed on mailto:kernel-info@redhat.com[kernel-info@redhat.com] or by filing a GitLab issue at https://gitlab.com/redhat/rhel/src/kernel/bugreports[Red Hat Kernel bug reports].

== Links to Important Docs and Tools

link:what_is_a_GitLab_fork.adoc[What is a GitLab Fork?] +
link:CommitRules.adoc[CommitRules, https://red.ht/kwf_commit_rules] +
link:RH_and_GitLab_Configuration.adoc[Red Hat and GitLab Configuration] +
link:lab.adoc[Gitlab ‘lab’ utility and the Red Hat Kernel] +
https://gitlab.cee.redhat.com/kernel-review/revumatic/[Red Hat Specific ‘revumatic’ tool]
https://gitlab.com/prarit/rhstatus[rhstatus]

== Useful Hints

1. Setup Red Hat Bugzilla Account
** https://bugzilla.redhat.com/createaccount.cgi[https://bugzilla.redhat.com/createaccount.cgi]
2. Setup link:RH_and_GitLab_Configuration[https://www.gitlab.com]
** Red SSO GitLab Login: https://red.ht/GitLabSSO[https://red.ht/GitLabSSO]
** Configure account access
*** SSH: https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account[https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account]
*** API Personal Access Token (PAT) for tools: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html[https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html]
*** Setup link:bichon.adoc[bichon]
*** Setup link:lab.adoc[lab]
*** Setup https://gitlab.com/prarit/rhstatus[rhstatus]
*** Setup https://gitlab.cee.redhat.com/kernel-review/revumatic[revumatic]
3. Important Red Hat Project projects
https://red.ht/GitLab[Main Red Hat RHEL namespace]
https://gitlab.com/redhat/rhel/src/kernel/rhel-8[Private rhel-8 kernel source]
https://gitlab.com/redhat/rhel/src/kernel/rhel-9[Private rhel-9 kernel source]
https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9[Public centos-stream-9 kernel source]

4. Clone Project

	git clone <project SSH>

5. Fork Project

	git clone <project SSH>; cd project_name
	lab fork # kernel repos take a long time to fork.  Grab a coffee.

* Find the name of the fork

	git remote -v  | grep <username>

6. Create an MR on main

**  *BEFORE CREATING A MERGE REQUEST READ THE* link:CommitRules.adoc[*CommitRules*] *AND* link:verifying_a_gitlab_MR.adoc[*VERIFY THE MR INFORMATION*]

	git checkout -b <branch_name>
	# do work
	git push -u <fork_name> <branch_name>
	lab mr create  [<origin>]

** Outputs a Merge Request URL that contains the MR ID
** It is strongly recommended lab users use the lab mr create --draft option to pass the webhook checks, and when successful, remove the Draft status on the MR with

	lab mr edit <mrID> --ready

7. link:create-a-merge-request-for-zstream.adoc[Create MR targeting a specific branch (i.e. z-stream)]

8. Get a list of MRs

	bichon # available on main page
	# or
	git fetch --all
	lab mr list --all

9. Checkout code from a MR

	git fetch --all
	lab mr list --all # to find the mrID
	lab mr checkout <mrID>

10. Get patches from a MR

	git fetch --all
	lab mr checkout <mrID>
	git-format-patch -<number_of_patches>
	# or
	git-format-patch origin/main

11. View code without checkout

	bichon #select MR from main page
	# or
	lab mr show --patch

12. Show comments on an MR

	bichon
	# or
	lab mr show <mrID> --comments

13. Comment on a MR

** Non-blocking

	bichon #select description, and add comment
	# or
	lab mr comment <mrID>

** Blocking (ie, NACK)

	bichon #select commit, add comment, select ‘Enable replies to comment’
	# or
	lab mr discussion <mrID>
	lab mr reply <mrID>:<comment_id>

14. Approve an MR

	bichon #select MR and ‘a’
	# or
	lab mr approve <mrID>

15. Unapprove an MR (ie, Rescind-Acked-by)

	bichon #select MR and ‘A’
	# or
	lab mr unapprove <mrID>

16. Close an MR

	lab mr close <mrID>

17. link:updating_or_fixing_a_MR.adoc[Updating or Fixing an MR]

== Tips and Tricks

. (Ab)use Draft/WIP state. Any Merge Request in Draft state will not generate emails to RHKL.
.. You can open a Merge Request in Draft state, and leave it in that state until you’re happy with everything -- CommitRefs checks, Signoff checks all pass, MR description looks sane, etc. Move the MR to Ready when you’re happy.
.. You can move a Merge Request from Ready back to Draft before you make updates to it. You can push additional commits, force-push modified commits, and update the MR description (add v2: notes) while it’s back in Draft state without generating any emails to RHKL, fix things up as needed, and then again, move to Ready to send emails. This use of Draft state also solves a problem where you want to update both commits and MR description, which when done while the MR is in Ready state, would send both a v2 and a v3.
.. Create your MRs early, in Draft state, and feel free to continuously update them with additional code from upstream until you’re happy with the progress. For example, update your driver to 5.12-rc1 code, but leave the MR in Draft state while you bring in additional code up through 5.12.0 (and maybe even stable releases).

